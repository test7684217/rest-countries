# [Countries Detail Application](https://rest-countries-drill.netlify.app/)

## Overview

This web application allows users to explore details about different countries, including their names, populations, currencies, and more. Users can search for specific countries, filter them by region, and toggle between light and dark themes for better readability.

## Features

- Search for countries by name.
- Filter countries by region.
- Toggle between light and dark themes.
- View detailed information about each country, including population, currencies, languages, etc.

## Technologies Used

- HTML5
- CSS3
- JavaScript (ES6+)

## External Libraries and APIs

- Font Awesome Icons (version 6.0.0-beta3)
- Google Fonts (Nunito Sans)



## Screenshots:

![]('./../images/light.png)

![]('./../images/dark.png)
