import { getData } from "./getData.js";

let listOfRegions = [];
let countriesData = [];
let regionFilter = "All";
let searchQuery = "";

//theme toggle
 document.addEventListener('DOMContentLoaded', () => {
  const theme = sessionStorage.getItem('theme');
  if (theme === 'dark') {
    document.body.classList.add('dark-theme');
  }
});

document.querySelector('.theme').addEventListener('click', () => {
  document.body.classList.toggle('dark-theme');
  const theme = document.body.classList.contains('dark-theme') ? 'dark' : 'light';
  sessionStorage.setItem('theme', theme);
});


let selectFilter = document.getElementById("select-filter");
selectFilter.addEventListener("change", (event) => {
  regionFilter = event.target.value;
  renderCards();
});

let countriesContainer = document.getElementsByClassName("countries-container");

let searchFilter = document.getElementById("search-input");

searchFilter.addEventListener("keydown", (event) => {
  searchQuery = event.target.value;
  renderCards();
});

getData()
  .then((data) => {
    // get all the regions
    let setOfRegions = new Set();
    data.forEach((item) => {
      setOfRegions.add(item.region);
    });

    listOfRegions = [...setOfRegions].sort();

    if (listOfRegions) {
      listOfRegions.forEach((region) => {
        let optionNode = document.createElement("option");
        optionNode.value = region;
        optionNode.innerText = region;
        selectFilter.appendChild(optionNode);
      });
    }

    return data;
  })
  .then((data) => {
    // get all countries
    countriesData = data.map((item, index) => {
      return {
        id: index,
        flag: item.flags.png,
        name: item.name.common,
        population: item.population,
        region: item.region,
        capital: item.capital ? item.capital[0] : "NA",
      };
    });
    renderCards();
    return data;
  });

function renderCards() {
  try {
    const filteredData = countriesData
      ?.filter((item) => {
        if (regionFilter == "All") return true;
        else if (regionFilter == item.region) return true;
      })
      .filter((item) => {
        if (item.name.toLowerCase().includes(searchQuery.toLowerCase()))
          return true;
      });

    countriesContainer[0].innerHTML = "";

    filteredData.forEach((item) => {
      let imgNode = document.createElement("img");
      imgNode.src = item.flag;

      let heading = document.createElement("h2");
      heading.innerText = item.name;

      let population = createInfoItem("population", item);

      let region = createInfoItem("region", item);

      let capital = createInfoItem("capital", item);

      let cardNode = document.createElement("div");
      cardNode.setAttribute("class", "card");
      cardNode.appendChild(imgNode);
      cardNode.appendChild(heading);
      cardNode.appendChild(population);
      cardNode.appendChild(region);
      cardNode.appendChild(capital);

      cardNode.addEventListener("click", () => {
        window.location.href = `details.html?id=${item.id}`;
      });

      countriesContainer[0].appendChild(cardNode);
    });
  } catch (error) {
    alert("Error in rendering country cards");
    alert(error);
  }
}

function createInfoItem(key, item) {
  try {
    let label = document.createElement("span");
    label.setAttribute("class", "label");

    let val = document.createElement("span");
    val.setAttribute("class", "value");

    label.innerText = `${key[0].toUpperCase() + key.substring(1)}: `;
    val.innerText = `${item[key]}`;

    let node = document.createElement("p");

    node.appendChild(label);
    node.appendChild(val);

    return node;
  } catch (error) {
    alert("Error in creating info item for cards");
    alert(error);
  }
}
