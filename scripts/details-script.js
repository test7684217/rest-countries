import { getData } from "./getData.js";
const urlParams = new URLSearchParams(window.location.search);
const countryId = urlParams.get("id");

const flag = document.getElementById("flag");
const countryHeading = document.getElementById("country-heading");
const nativeName = document.getElementById("native-name");
const population = document.getElementById("population");
const region = document.getElementById("region");
const subregion = document.getElementById("subregion");
const capital = document.getElementById("capital");
const topLevelDomain = document.getElementById("topleveldomain");
const currencies = document.getElementById("currencies");
const languages = document.getElementById("languages");
const goBack = document.querySelector("#go-back");
const borderCountriesContainer = document.getElementById("border-countries");


// theme toggle

document.addEventListener('DOMContentLoaded', () => {
  const theme = sessionStorage.getItem('theme');
  if (theme === 'dark') {
    document.body.classList.add('dark-theme');
  }
});

document.querySelector('.theme').addEventListener('click', () => {
  document.body.classList.toggle('dark-theme');
  const theme = document.body.classList.contains('dark-theme') ? 'dark' : 'light';
  sessionStorage.setItem('theme', theme);
});

1

getData().then((data) => {
  let mappedData = data.map((item, index) => {
    return {
      id: index,
      item,
    };
  });

  let country = mappedData.find((item) => item.id == countryId);

  if (country) {
    flag.src = country.item.flags.png;
    countryHeading.innerText = country.item.name.common;
    const nativeNamesKeys = Object.keys(country.item.name.nativeName);
    const nativeName1 = nativeNamesKeys[0];
    nativeName.innerText =
      country.item.name.nativeName[nativeName1].common || "N/A";
    population.innerText = country.item.population;
    region.innerText = country.item.region;
    subregion.innerText = country.item.subregion || "N/A";
    capital.innerText = country.item.capital || "N/A";
    topLevelDomain.innerText = country.item.tld?.join(", ") || "N/A";

    const currencyList = Object.keys(country.item.currencies);
    currencies.innerText =
      currencyList.map((currency) => currency)?.join(", ") || "N/A";

    const languageList = Object.values(country.item.languages);

    languages.innerText =
      languageList.map((language) => language).join(", ") || "N/A";

    let borderCountries = getCountry(country.item.borders, mappedData);

    renderBorderCountries(borderCountries);
  } else {
    console.error("Country not found");
  }
});

goBack.addEventListener("click", () => {
  window.history.back();
});

function getCountry(list, data) {
  try {
    let set = new Set();

    list?.forEach((element) => {
      set.add(element);
    });

    return data?.reduce((acc, element) => {
      if (set.has(element.item.cca3)) {
        acc.push(element);
      }
      return acc;
    }, []);
  } catch (error) {
    alert(error);
    alert("error finding countries by code");
  }
}

function renderBorderCountries(list) {
  try {
    if (list.length) {
      let label = document.createElement("p");
      label.setAttribute("class", "label-name");
      label.innerText = "Border Countries: ";
      borderCountriesContainer.appendChild(label);

      list.forEach((item) => {
        let country = document.createElement("p");
        country.addEventListener("click", () => {
          window.location.href = `details.html?id=${item.id}`;
        });
        country.setAttribute("class", "border-countries-text");
        country.innerText = item.item.name.common;

        borderCountriesContainer.appendChild(country);
      });
    }
  } catch (error) {
    alert(error);
    alert("Error render border render");
  }
}
