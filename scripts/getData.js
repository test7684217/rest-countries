let loader = document.getElementById('loader');

export async function getData() {
    try {

      loader.classList.add('show');
      loader.classList.remove('hide');

      const response = await fetch("https://restcountries.com/v3.1/all", {
        method: "GET",
      });
      const data = await response.json();
  
      loader.classList.remove('show');
      loader.classList.add('hide');
      return data;
    } catch (error) {
      alert("Error in fetching data");
      alert(error);
    }
  }
